# green-recorder

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

Simple screen recorder for Linux desktop, supports Wayland & Xorg

https://github.com/dvershinin/green-recorder

<br><br>
How to clone this repo:

```
git clone https://gitlab.com/azul4/content/gnome/green-recorder.git
```
